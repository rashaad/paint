var intervalGetWorld, intervalGetWorldIDs;
var currentWorldID="";


function log(str){
	console.log(str);
}

function getPosition(){
	if (navigator.geolocation){
		navigator.geolocation.getCurrentPosition(initialize);
	}else{
		console.log("Geolocation is not supported by this browser.");
	}
}

function initialize(position) {
	var mapOptions = {
		center: new google.maps.LatLng(position.coords.latitude, position.coords.longitude),
		zoom: 8,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};

    var map = new google.maps.Map(document.getElementById("map-world"),mapOptions);
  
    marker1 = new google.maps.Marker({
    	position: new google.maps.LatLng(position.coords.latitude, position.coords.longitude), 
    	map: map,
    	icon: 'http://upload.wikimedia.org/wikipedia/commons/6/67/Red_Dot.svg'
    });
    console.log(marker1.position);
    marker2 = new google.maps.Marker({
    	position: new google.maps.LatLng(42, -79), 
    	map: map,
    	icon: 'http://upload.wikimedia.org/wikipedia/commons/6/67/Red_Dot.svg'
    });

    map.setCenter(marker1.getPosition());

	google.maps.event.addDomListener(window, 'resize', function() {
	  map.setCenter(marker1.getPosition());
	});
}

function clearSelect(element){

	$(element).html("");
}

function getWorldIDs(element){

	element = $(element);
	$.getJSON("getWorldIDs.php", {}, function(data){
		if(data){
			clearSelect(element);
			$.each(data, function(index, value){

				$(element).append("<li><a class=\"existing-world\" onclick=\"switchWorld('" + value + "');\" href=\"#\">" + value + "</a></li>");
			});
		}	
	});

}

function createNewWorld(){
	var newWorldID=$("#userWorldName")[0].value;
	$.getJSON("saveWorld.php", { "worldID" : newWorldID }, function(data){
			switchWorld(newWorldID);
	});
}

function selectWorld(element){
	if(element){
		console.log($(element).html());
		switchWorld($(element).html());
	}
}

function switchWorld(whichWorld){
	
	currentWorldID=whichWorld;
	resetWorld();
}

function getWorld(){
	$.getJSON("e8get.php", { "worldID" : currentWorldID }, function(data){
		// console.log(data);
		$.each(data, function(index, value){
			var item_collection=$("#"+index);
			var item=item_collection[0];
			if(!item.isDragging && item.sequenceNumber<value.sequenceNumber){
				// item_collection.css({ "left": value.x+"px", "top": value.y+"px", });
				item_collection.animate({ "left": value.x+"%", "top": value.y+"%", });
				item.sequenceNumber=value.sequenceNumber;
			}
		});
	});

}
function setObject(event, ui){
	var update = { "world" : currentWorldID };
	var leftInPercentages=ui.position.left/$("#world").width()*100;
	var topInPercentages=ui.position.top/$("#world").height()*100;
	update[event.target.id]= {
		"x": leftInPercentages,
		"y": topInPercentages,
		"sequenceNumber" : event.target.sequenceNumber
	};
	var updateString=JSON.stringify(update);
	$.getJSON("e8set.php", { update: updateString }, function(data){
		// console.log(JSON.stringify(data)); 
	});
}
function resetWorld() {
	window.clearInterval(intervalGetWorld);
	if(currentWorldID!=""){
		$("#world img").css({ "display" : "inline", "left" : "5%", "top":"15%", });

		$("#world img").each(function (index, e){
			e.sequenceNumber=0;
			e.isDragging=false;
		});


		$("#worldLabel").html(currentWorldID);
		intervalGetWorld = window.setInterval(getWorld, 1000);
	} else {
		$("#world img").css({ "display" : "none" });
	}
	window.clearInterval(intervalGetWorldIDs);
	//intervalGetWorldIDs = window.setInterval(getWorldIDs, 5000);
}

$(function(){

	//$("#tools").draggable({ containment: "parent" });

	window.onresize = function() {
		log($('#world').width());
		$("#world").width($('#world-container').width());
		$("#world").height(window.innerHeight - 100);
	};

	$("#world").width($('#world-container').width());
	$("#world").height(window.innerHeight - 150);

	getPosition();

	$('.btn').mouseover(function(){
		$(this).tooltip('show');
	});

	// world stuff
	$("#world img").draggable({
		containment: $('#world')
	}); 
	$("#world img").on("dragstart", function(event, ui) { 
		event.target.isDragging=true;
	});
    $("#world img").on("drag"     , function(event, ui) { 
		event.target.isDragging=true;
	});
    $("#world img").on("dragstop" , function(event, ui) { 
		event.target.isDragging=false;
		setObject(event, ui); 
	});
	resetWorld();

	log($(".existing-world"));


});

//google.maps.event.addDomListener(window, 'load', getPosition);

