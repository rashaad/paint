
// Initial starter code by Arnold Rosenbloom, for CSC309, Spring 2013

function randint(n){
	return Math.round(Math.random()*n);
}

function Paint(canvas){
	this.canvas=canvas; // The canvas this instance of Paint is working on
	this.context=this.canvas.getContext("2d");

 	// Strategy design pattern. 
	// strategy is used to map some canvas events to operations on a command being constructed
	// change strategies to construct a SquiggleCommand, a RectangleCommand, a CircleCommand, ...
	this.strategy=null;

	// The Command design pattern
 	// a list of commands which can be used to repaint the whole canvas. 
	this.commands=[];

	this.resize();

	//Settings variables
	this.fillColor = "rgba(0,0,0,1)"; //fill color
	this.strokeColor = "rgba(0,0,0,1)"; //stroke color
	this.lineJoinType = 'miter'; //type of join for lines
	this.lineThicknessEnabled = true;
	this.lineThickness = 1; //thickness of stroke
	this.closePath = true;

	//create the color picker
	this.picker = new ColorPicker('color-picker', 'color-preview', 'color-picked', 'images/color_picker.png', 256,256);

	//enable ctrl+z shortcut
	this.enableShortcuts();

	//turn on undo listener
	var self = this;
	document.addEventListener("CTRL+Z", function(event){ self.undo(); }, true);
}

//Start listening for shortcuts
Paint.prototype.enableShortcuts=function(){
	var self = this;
	document.onkeydown = function(event){self.detectShortcut(event);};
	document.onkeyup = function(event){self.detectShortcutRelease(event);};
}

//Main keydown shortcut listener
Paint.prototype.detectShortcut=function(evt){
	//http://stackoverflow.com/questions/1727812/how-to-detect-when-a-shortcut-key-is-pressed-in-javascript
	//new custom shortcut event
	var e = document.createEvent("Event");
	
	if (!evt) evt = event;
	//console.log(evt.keyCode);
	if (evt.ctrlKey && evt.keyCode==90){ //CTRL+Z
		console.log("CTRL+Z");
		e.initEvent("CTRL+Z", true, true);
		document.dispatchEvent(e);
	}else if(evt.shiftKey){
		console.log("SHIFT");
		e.initEvent("SHIFT", true, true);
		document.dispatchEvent(e);
	}
}

//Detects when a shortcut has been released
Paint.prototype.detectShortcutRelease=function(evt){
	
	var e = document.createEvent("Event");
	if (!evt) evt = event;
	if(evt.keyCode == 16){
		console.log("SHIFTUP");
		e.initEvent("SHIFTUP", true, true);
		document.dispatchEvent(e);
	}else if(evt.keyCode == 27){
		console.log("ESC");
		e.initEvent("ESC", true, true);
		document.dispatchEvent(e);
	}
}

Paint.prototype.setStrategy=function(strategy){
	this.strategy = strategy;
}

//Listens for events being dispatched from the color picker
Paint.prototype.listenForFillChange=function(callback){
	var self = this;
	//update the picker
	this.picker.updatePicked(this.fillColor);
	//alter the dispatching event so that ONLY we catch it
	this.picker.setEventName("colorPickedFill");
	//listen for fill color being picked
	document.addEventListener("colorPickedFill", function(event){
		self.setFill(event.color);
		callback.style.backgroundImage = "none";
		callback.style.backgroundColor = event.color;
	});
}

Paint.prototype.listenForStrokeChange=function(callback){
	var self = this;
	//update the stroke
	this.picker.updatePicked(this.strokeColor);
	//alter the dispatching event so that it gets caught HERE
	this.picker.setEventName("colorPickedStroke");
	//listen for fill color being picked
	document.addEventListener("colorPickedStroke", function(event){
		self.setStroke(event.color);
		callback.style.backgroundImage = "none";
		callback.style.backgroundColor = event.color;
	});
}

Paint.prototype.setClosePath=function(element){
	if(element && element.checked){
		this.closePath = true;
	}else{
		this.closePath = false;
	}
}

Paint.prototype.getClosePath=function(){
	return this.closePath;
}

//Setter for fill color
Paint.prototype.setFill=function(color){
	this.fillColor = color;
}

//Setter for stroke color
Paint.prototype.setStroke=function(color){
	this.strokeColor = color;
}

//Getter for stroke
Paint.prototype.getStroke=function(){
	return this.strokeColor;
}

//Getter for fill
Paint.prototype.getFill=function(){
	return this.fillColor;
}

//Set the line join type i.e. miter, bevel, etc...
Paint.prototype.setJoin=function(element){
	this.lineJoinType = element.value;
	console.log("Join type set to: " + this.lineJoinType);
}

//Getter for the line join type
Paint.prototype.getJoin=function(){
	return this.lineJoinType;
}

//Set the line thickness
Paint.prototype.setLineThickness=function(element){
	var val = parseInt(element.value) || 0;
	console.log("thickness is :" + (val > 0));
	if(val > 0){
		this.lineThickness = element.value;
		this.lineThicknessEnabled = true;
	}else{
		this.lineThickness = null;
	}
}

//Getter for line thickness
Paint.prototype.getLineThickness=function(){
	if(this.lineThicknessEnabled){
		return this.lineThickness;	
	}
	return null;
}

//render an element visible
Paint.prototype.show=function(element){
	document.getElementById(element).style.display = "inline";
}

//render an element invisible
Paint.prototype.hide=function(element){
	document.getElementById(element).style.display = "none";
}

Paint.prototype.getContext=function(){
	return this.context;
}

Paint.prototype.draw=function(){
	this.resize();
	for(var i=0;i<this.commands.length;i++){
		this.commands[i].draw(this.context);
	}
}

Paint.prototype.resize=function() {
	this.canvas.width = window.innerWidth - 50;
	this.canvas.height = window.innerHeight - 100;
}

Paint.prototype.undo=function(){
	// context.clearRect ( 0 , 0 , this.width , this.height ); // clear the canvas
	this.canvas.width=this.canvas.width; // clear the canvas
	this.commands.pop();
	this.draw();
}

Paint.prototype.clear=function(){
	this.commands=[];
	this.draw();
}

//INCOMPLETE....cannot stringify state
Paint.prototype.save=function(filename){
	//http://stackoverflow.com/questions/11214404/how-to-detect-if-browser-supports-html5-local-storage
	//http://stackoverflow.com/questions/1240072/how-do-i-strip-bad-chars-from-a-string-in-js
	if(typeof(Storage)!=="undefined" && this.commands.length > 0){
		var date = new Date();
		
		filename = filename.replace(/[^a-z 0-9]+/gi,'') + date.getFullYear() + '-' + date.getMonth() + '-' + date.getDate();

		var num = localStorage.getItem('numPaints') || 0;
		localStorage.setItem(filename, JSON.stringify(this.commands));
		localStorage.setItem('numPaints', parseInt(num) + 1);
		console.log(filename + " was saved.");

	}else{
		console.log("Data was not saved");
	}
}

//INCOMPLETE....cannot stringify state
Paint.prototype.getSavedItems=function(element){
	//console.log("working");
	if(typeof(Storage)!=="undefined" && element.length > 0){
		if(localStorage.length > 0){
			var num = parseInt(localStorage.getItem('numPaints') || 0);
			for(var i=0; i < num; i++){
				var opt = document.createElement("option");
				opt.value = localStorage.key(i);
				opt.text = localStorage.key(i);
				element.add(opt);
			}
		}
	}else{
		console.log("Data was not saved");
	}
}
//INCOMPLETE....cannot stringify state
Paint.prototype.load=function(filename){
	//console.log(localStorage.key(i));
	//console.log(localStorage.getItem();
	var self = this;
	if(typeof(Storage)!=="undefined"){
		if(localStorage.length > 0 && localStorage.getItem(filename) != null){
			this.commands = JSON.parse(localStorage.getItem(filename));
			//console.log(self);
			this.draw();
		}
	}else{
		console.log("Data was not saved");
	}
}

Paint.prototype.mapToCanvas=function(e){
	// From: http://www.html5canvastutorials.com/advanced/html5-canvas-mouse-coordinates/
	var rect = this.canvas.getBoundingClientRect();
	// decided I would modify the event, by adding some attributes
       e.canvasX=e.clientX - rect.left;
       e.canvasY=e.clientY - rect.top;
	return e;
}

/** A strategy used to capture events and construct a Squiggle command **/
function SquiggleStrategy(paint){
	this.command=null; // the current command this is building
	this.paint=paint; // the instance of paint this.command is being added to

	//listen for stroke changes
	//document.addEventListener("strokeChange", )
}

SquiggleStrategy.prototype.mousedown=function(event){
	// Create a new SquiggleCommand, for now, just create a random
	// line description, you should modify this so that the line description
	// comes from any user interface elements.
	if(this.paint.getStroke() != null && this.paint.getLineThickness()){
		this.command=new SquiggleCommand(this.paint.getStroke(), this.paint.getLineThickness());
		this.command.addPoint({x:event.canvasX, y:event.canvasY});
		this.paint.commands.push(this.command);
	}
}

SquiggleStrategy.prototype.mouseup=function(event){
	if(this.command!=null){
		this.command.draw(this.paint.context);
		this.command=null;
	}
} 

SquiggleStrategy.prototype.mousemove=function(event){
	if(this.command!=null){
		this.command.addPoint({x:event.canvasX, y:event.canvasY});
		this.command.draw(this.paint.context);
	}
}

SquiggleStrategy.prototype.mouseout=function(event){
	this.mouseup(event);
}

function SquiggleCommand(strokeStyle, lineWidth){
	this.strokeStyle=strokeStyle;
	this.lineWidth=lineWidth;
	//check if they are trying to draw a blank line
	if(!this.lineWidth){
		this.lineWidth = 1;
	}
	this.points=[];
}

SquiggleCommand.prototype.addPoint=function(point){
	this.points.push(point);
}

SquiggleCommand.prototype.draw=function(context){
	// All commands understand draw, paint may ask us to do this.

	if(this.points.length==0){
		return;
	}
	context.beginPath(); 
	//Pen can never be null
	if(this.strokeStyle == null){
		this.strokeStyle = 'black';
	}
	context.strokeStyle = this.strokeStyle;
	context.lineWidth=this.lineWidth;
	context.moveTo(this.points[0].x,this.points[0].y);
	for (i = 1; i < this.points.length - 2; i++) {
		//compute delta x/2
        var c = (this.points[i].x + this.points[i + 1].x) / 2;
        //compute delta y/2
        var d = (this.points[i].y + this.points[i + 1].y) / 2;
        //
        context.quadraticCurveTo(this.points[i].x, this.points[i].y, c, d);
    }
	context.stroke();
}

/** A strategy used to capture events and construct a Squiggle command **/
function RectStrategy(paint){
	this.command = null;
	this.paint = paint; // the instance of paint this.command is being added to
	this.constraintProportions = false;
}

RectStrategy.prototype.mousedown=function(event){
	if(this.paint.getStroke() || this.paint.getFill()){
		this.command = new RectCommand(this.paint.getStroke(), this.paint.getFill(), this.paint.getLineThickness());
		this.command.begin({x:event.canvasX, y:event.canvasY});
		this.paint.commands.push(this.command);
	}
}

RectStrategy.prototype.mouseup=function(event){
	this.command=null;
} 

RectStrategy.prototype.mousemove=function(event){

	var ghostX = event.canvasX;
	var ghostY = event.canvasY;
	var self = this;
	//if SHIFT is held down, proportions are constraint
	document.addEventListener("SHIFT", function(event){self.constraintProportions = true;});
	//turn off proportion constraint
	document.addEventListener("SHIFTUP", function(event){self.constraintProportions = false;});
	
	if(this.command){
		this.paint.undo();
		this.command.end({x:ghostX, y:ghostY});

		//check if proportions need to be constraint
		//take the smallest of width/height and use that the length of the square
		//new endpoint will be computed
		if(this.constraintProportions){
			if(this.command.getWidth() > this.command.getHeight()){
				ghostX = ghostX - this.command.getWidth() + this.command.getHeight();
			}else if(this.command.getHeight() > this.command.getWidth()){
				ghostY = ghostY - this.command.getHeight() + this.command.getWidth();
			}
			this.command.end({x:ghostX, y:ghostY});	
		}
		this.command.draw(this.paint.context);
		this.paint.commands.push(this.command);
	}
}

RectStrategy.prototype.mouseout=function(event){
	if(this.command){
		this.mouseup(event);	
	}
}

function RectCommand(strokeStyle, fillStyle, lineWidth){
	this.strokeStyle = strokeStyle;
	this.fillStyle = fillStyle;
	this.lineWidth = lineWidth;
	this.startPoint = null;
	this.endPoint = null;

	//both fill and linewidth cannot be null so defaul linewidth to 1 in that event
	if(this.fillStyle == null && this.lineWidth == null){
		this.lineWidth = 1;
		this.strokeStyle = 'black';
	}
}

RectCommand.prototype.begin=function(point){
	this.startPoint = point;
}

RectCommand.prototype.end=function(point){
	this.endPoint = point;
}

RectCommand.prototype.getWidth=function(){
	return this.endPoint.x - this.startPoint.x;
}

RectCommand.prototype.getHeight=function(){
	return this.endPoint.y - this.startPoint.y;
}

RectCommand.prototype.draw=function(context) {
	if(this.startPoint != null && this.endPoint != null){
		context.beginPath();
		context.rect(this.startPoint.x, this.startPoint.y, this.getWidth(), this.getHeight());

		if(this.fillStyle){
			context.fillStyle = this.fillStyle;
			context.fill();	
		}
		if(this.lineWidth && this.strokeStyle){
			context.lineWidth = this.lineWidth;
			context.strokeStyle = this.strokeStyle;
			context.stroke();
		}
	}
}

/** A strategy used to capture events and construct a Triangle command **/
function TriangleStrategy(paint){
	this.command = null;
	this.paint = paint; // the instance of paint this.command is being added to
	this.constraintProportions = false;
}

TriangleStrategy.prototype.mousedown=function(event){
	if(this.paint.getStroke() || this.paint.getFill()){
		this.command = new TriangleCommand(this.paint.getStroke(), this.paint.getFill(), this.paint.getJoin(), this.paint.getLineThickness());
		this.command.begin({x:event.canvasX, y:event.canvasY});
		this.paint.commands.push(this.command);
	}
}

TriangleStrategy.prototype.mouseup=function(event){
	this.command=null;
} 

TriangleStrategy.prototype.mousemove=function(event){

	var ghostX = event.canvasX;
	var ghostY = event.canvasY;
	var self = this;

	//if SHIFT is held down, proportions are constraint
	document.addEventListener("SHIFT", function(event){self.constraintProportions = true;});
	//turn off proportion constraint
	document.addEventListener("SHIFTUP", function(event){
		self.constraintProportions = false;
	});

	if(this.command){
		this.paint.undo();
		this.command.end({x:ghostX, y:ghostY});
		//check if proportions need to be constraint
		//take the smallest of width/height and use that the length of the square
		//new endpoint will be computed
		if(this.constraintProportions){
			if(this.command.getWidth() > this.command.getHeight()){
				//we are constraining by height then
				var startPoint = this.command.getStart();
				ghostX = startPoint.x + (this.command.getHeight() / Math.cos(30 * Math.PI/180));
			}else if(this.command.getHeight() > this.command.getWidth()){
				var startPoint = this.command.getStart();
				ghostY = startPoint.y + (this.command.getWidth() * Math.cos(30 * Math.PI/180));
			}
			this.command.end({x:ghostX, y:ghostY});	
		}

		this.command.draw(this.paint.context);
		this.paint.commands.push(this.command);
	}
}

TriangleStrategy.prototype.mouseout=function(event){
	if(this.command){
		this.mouseup(event);	
	}
}

function TriangleCommand(strokeStyle, fillStyle, lineJoinStyle, lineWidth){
	this.strokeStyle = strokeStyle;
	this.fillStyle = fillStyle;
	this.lineWidth = lineWidth;
	this.joinStyle = lineJoinStyle;
	this.startPoint = null;
	this.endPoint = null;

	//both fill and linewidth cannot be null so defaul linewidth to 1 in that event
	if(this.fillStyle == null && this.lineWidth == null){
		this.lineWidth = 1;
		this.joinStyle = 'miter';
		this.strokeStyle = 'black';
	}
}

TriangleCommand.prototype.begin=function(point){
	this.startPoint = point;
}

TriangleCommand.prototype.getStart=function(){
	return this.startPoint;
}

TriangleCommand.prototype.end=function(point){
	this.endPoint = point;
}

TriangleCommand.prototype.getWidth=function(){
	return this.endPoint.x - this.startPoint.x;
}

TriangleCommand.prototype.getHeight=function(){
	return this.endPoint.y - this.startPoint.y;
}

TriangleCommand.prototype.draw=function(context) {
	if(this.startPoint != null && this.endPoint != null){
		context.beginPath();
		//move to the lower left point
		context.moveTo(this.startPoint.x, this.endPoint.y);
		//move the apex point
		context.lineTo(this.startPoint.x + (this.getWidth() / 2), this.startPoint.y);
		//move the lower right point
		context.lineTo(this.endPoint.x, this.endPoint.y);
		//move to the lower left point
		context.lineTo(this.startPoint.x, this.endPoint.y);
		//move back to the apex for the miter to join properly
		context.lineTo(this.startPoint.x + (this.getWidth() / 2), this.startPoint.y);
		//join the lines i.e. miter, rounded, etc
		if(this.fillStyle){
			context.fillStyle = this.fillStyle;
			context.fill();
		}
		if(this.lineWidth && this.strokeStyle){
			context.lineWidth = this.lineWidth;
			context.lineJoin = this.joinStyle;
			context.strokeStyle = this.strokeStyle;
			context.stroke();
		}
	}
}

/** A strategy used to capture events and construct a Triangle command **/
function EllipseStrategy(paint){
	this.command = null;
	this.paint = paint; // the instance of paint this.command is being added to
	this.constraintProportions = false;
}

EllipseStrategy.prototype.mousedown=function(event){
	if(this.paint.getStroke() || this.paint.getFill()){
		this.command = new EllipseCommand(this.paint.getStroke(), this.paint.getFill(), this.paint.getLineThickness());
		this.command.begin({x:event.canvasX, y:event.canvasY});
		this.paint.commands.push(this.command);
	}
}

EllipseStrategy.prototype.mouseup=function(event){
	this.command=null;
} 

EllipseStrategy.prototype.mousemove=function(event){
	var ghostX = event.canvasX;
	var ghostY = event.canvasY;
	var self = this;
	//if SHIFT is held down, proportions are constraint
	document.addEventListener("SHIFT", function(event){self.constraintProportions = true;});
	//turn off proportion constraint
	document.addEventListener("SHIFTUP", function(event){self.constraintProportions = false;});

	if(this.command){
		this.paint.undo();
		this.command.end({x:ghostX, y:ghostY});

		//check if proportions need to be constraint
		//take the smallest of width/height and use that the length of the square
		//new endpoint will be computed
		if(this.constraintProportions){
			if(this.command.getWidth() > this.command.getHeight()){
				ghostX = ghostX - this.command.getWidth() + this.command.getHeight();
			}else if(this.command.getHeight() > this.command.getWidth()){
				ghostY = ghostY - this.command.getHeight() + this.command.getWidth();
			}
			this.command.end({x:ghostX, y:ghostY});	
		}
		this.command.draw(this.paint.context);
		this.paint.commands.push(this.command);
	}
}

EllipseStrategy.prototype.mouseout=function(event){
	if(this.command){
		this.mouseup(event);	
	}
}

function EllipseCommand(strokeStyle, fillStyle, lineWidth){
	this.strokeStyle = strokeStyle;
	this.fillStyle = fillStyle;
	this.lineWidth = lineWidth;
	this.startPoint = null;
	this.endPoint = null;

	//both fill and linewidth cannot be null so defaul linewidth to 1 in that event
	if(this.fillStyle == null && this.lineWidth == null){
		this.lineWidth = 1;
		this.strokeStyle = 'black';
	}
}

EllipseCommand.prototype.begin=function(point){
	this.startPoint = point;
}

EllipseCommand.prototype.end=function(point){
	this.endPoint = point;
}

EllipseCommand.prototype.getWidth=function(){
	return this.endPoint.x - this.startPoint.x;
}

EllipseCommand.prototype.getHeight=function(){
	return this.endPoint.y - this.startPoint.y;
}

EllipseCommand.prototype.getMidPoint=function(){
	return {x:this.startPoint.x + (this.getWidth()/2), y:this.startPoint.y + (this.getHeight()/2)};
}

EllipseCommand.prototype.getRadius=function(){
	return Math.abs(Math.min(this.getWidth(), this.getHeight()));
}

EllipseCommand.prototype.getScale=function(){
	if(this.getHeight() > this.getWidth()){
		//need to scale width by a factor of height
		return {scaleX:1, scaleY:this.getHeight() / this.getWidth()};
	}else if(this.getHeight() < this.getWidth()){
	    return {scaleX:this.getWidth() / this.getHeight(), scaleY:1};
	}
	return {scaleX:1, scaleY:1};
}

EllipseCommand.prototype.draw=function(context) {
	if(this.startPoint != null && this.endPoint != null){
		mid = this.getMidPoint();
		// save the current state of the canvas
		context.save();
		// scale context horizontally
		scale = this.getScale();
		context.scale(scale.scaleX, scale.scaleY);
		// draw circle which will be stretched into an oval
		context.beginPath();
		//arc(x, y, radius, startAngle, endAngle, antiClockwise);
		context.arc(mid.x / scale.scaleX , mid.y / scale.scaleY, this.getRadius()/2, 0, 2 * Math.PI, false);
		// restore to original state
		context.restore();

		if(this.fillStyle){
			context.fillStyle = this.fillStyle;
			context.fill();
		}
		if(this.lineWidth && this.strokeStyle){
			context.lineWidth = this.lineWidth;
			context.strokeStyle = this.strokeStyle;
			context.stroke();
		}
	}
}

/** A strategy used to capture events and construct a Triangle command **/
function PolygonStrategy(paint){
	this.command = null;
	this.paint = paint; // the instance of paint this.command is being added to
}

PolygonStrategy.prototype.mousedown=function(event){
	//check for mouse 3
	if(event.button == 1){
		self.finish();
		return;
	}
	//check for ESC key
	var self = this;
	document.addEventListener("ESC", function(event){
		self.finish();
	});

	if(this.command){
		this.command.addPoint({x:event.canvasX, y:event.canvasY});
		this.command.draw(this.paint.context);
	}else{
		if(this.paint.getStroke() || this.paint.getFill()){
			this.command = new PolygonCommand(this.paint.getStroke(), this.paint.getFill(), this.paint.getJoin(), this.paint.getLineThickness(), this.paint.getClosePath());
			this.command.addPoint({x:event.canvasX, y:event.canvasY});
			this.paint.commands.push(this.command);
			this.command.draw(this.paint.context);
		}	
	}
}

PolygonStrategy.prototype.mouseup=function(event){
	//this.command=null;
} 

PolygonStrategy.prototype.mousemove=function(event){
	if(this.command && this.command.points.length > 0){
		if(this.command.points.length > 1){
			this.paint.undo();
			this.command.popPoint();
		}
		this.command.addPoint({x:event.canvasX, y:event.canvasY});
		this.command.draw(this.paint.context);
		this.paint.commands.push(this.command);
	}
}

PolygonStrategy.prototype.mouseout=function(event){
	if(this.command){
		this.finish();
	}
}

PolygonStrategy.prototype.finish=function(){
	this.command.popPoint();
	this.paint.undo();
	this.command.draw(self.paint.context);
	this.command = null;
}

function PolygonCommand(strokeStyle, fillStyle, lineJoinStyle, lineWidth, closePath){
	this.strokeStyle = strokeStyle;
	this.fillStyle = fillStyle;
	this.lineWidth = lineWidth;
	this.joinStyle = lineJoinStyle;
	this.closePath = closePath;
	this.startPoint = null;
	this.endPoint = null;
	this.points = [];

	//both fill and linewidth cannot be null so defaul linewidth to 1 in that event
	if(this.fillStyle == null && this.lineWidth == null){
		console.log("Polygon Defaulting..");
		this.lineWidth = 1;
		this.joinStyle = 'miter';
		this.strokeStyle = 'black';
	}
}

PolygonCommand.prototype.addPoint=function(point){
	this.points.push(point);
}

PolygonCommand.prototype.popPoint=function(){
	return this.points.pop();
}

PolygonCommand.prototype.showTrail=function(point){

}

PolygonCommand.prototype.draw=function(context) {
	if(this.points.length > 1){
		context.beginPath();
		//move to the lower left point
		context.moveTo(this.points[0].x, this.points[0].y);

		for(var i=1; i < this.points.length; i++){
			//move the apex point
			context.lineTo(this.points[i].x, this.points[i].y);
		}

		if(this.lineWidth && this.strokeStyle){
			context.strokeStyle = this.strokeStyle;
			context.lineWidth = this.lineWidth;
			context.lineJoin = this.joinStyle;
			if(this.closePath){
				context.closePath();
			}
			context.stroke();
		}
		if(this.fillStyle){
			context.fillStyle = this.fillStyle;
			context.fill();
		}
	}
}

function ColorPicker(pickerId, pickerPreviewId, pickerPickedId, pickerSrc, width, height){
	this.canvas = document.getElementById(pickerId);
	this.pickerId = pickerId;
	this.pickerPreview = document.getElementById(pickerPreviewId);
	this.pickerPicked = document.getElementById(pickerPickedId);
	this.pickerSrc = pickerSrc;
	this.context = this.canvas.getContext("2d");
	this.loadColorPicker(pickerSrc);
	this.width = width;
	this.height = height;
	//currently picked color
	this.color = 'rgba(0,0,0,1)';
	this.red = 255;
	this.green = 255;
	this.blue = 255;
	this.alpha = 255;

	//add listeners
	var self = this;
	this.canvas.addEventListener("mouseup", function(event){ self.pick(event); }, true);
	this.canvas.addEventListener("mousemove", function(event){ self.preview(event); }, true);
	this.customEventName = 'colorPicked';
}

//Set the name of the event to be dispatched
ColorPicker.prototype.setEventName=function(name){
	this.customEventName = name;
}

//setup the color picker
ColorPicker.prototype.loadColorPicker=function(pickerSrc){
	var imageObj = new Image();
	var self = this;
	imageObj.onload = function(){
		self.context.drawImage(this, 0, 0);
	};
	imageObj.src = pickerSrc;
}
//Constantly keep previewing the color at the mouse location
ColorPicker.prototype.preview=function(event){
	var currentColor = this.getColor(event);
	this.pickerPreview.style.backgroundColor = currentColor;
	//this.pickerPreview.innerHTML = "<p>r: " + this.red + "</p> <p>g: " + this.green + "</p> <p>b: " + this.blue + "</p>";
}

//When the user clicks mouseup on a region, pick that color
ColorPicker.prototype.pick=function(event){
	this.color = this.getColor(event);
	this.pickerPicked.style.backgroundColor = this.color;
	this.pickerPicked.innerHTML = "<p>R: " + this.red + "</p> <p>G: " + this.green + "</p> <p>B: " + this.blue + "</p>";

	//dispatch an event notifying any listeners that the picker has changed color
	var e = document.createEvent("Event");
	e.initEvent(this.customEventName, true, true);
	e.color = this.color;
	document.dispatchEvent(e);
}

ColorPicker.prototype.updatePicked=function(color){
	this.pickerPicked.style.backgroundColor = color;
}

//Get the color at an x,y coordinate of the mouse
ColorPicker.prototype.getColor=function(event){
	var rect = this.canvas.getBoundingClientRect();
	//get the 1x1 data sample from where the mouse is
	var pixelData = this.context.getImageData(event.clientX - rect.left, event.clientY - rect.top, 1,1);
	this.red = pixelData.data[0];
	this.green = pixelData.data[1];
	this.blue = pixelData.data[2];
	this.alpha = pixelData.data[3];
	return "rgba(" + this.red + ',' + this.green + ',' + this.blue + ',' + this.alpha + ')';
}

/*
* Toggles menu buttons
*/
function navToggler(elements){
	//current toggled item
	var toggled = null;
	for(var i=0; i < elements.length; i++){
		elements[i].addEventListener("click", function(event){
			if(toggled){
				toggled.className = "tool";
			}
			this.className = "tool hover";
			toggled = this;
		});	
	}
}

//Helper function responsible for hiding/showing options for the 
//currently selected tool
function updateTools(element, paint){
	//paint.enableCtrlZ();
	if(element.id == "draw-rect"){
		paint.hide('join-setting');
		paint.show('fill-setting');
		paint.show('stroke-setting');
		paint.show('line-thickness-setting');
		paint.hide('close-path-setting');
	}else if(element.id == "draw-line"){
		paint.hide('join-setting');
		paint.hide('fill-setting');
		paint.show('stroke-setting');
		paint.show('line-thickness-setting');
		paint.hide('close-path-setting');
	}else if(element.id == "draw-triangle"){
		paint.show('line-thickness-setting');
		paint.show('stroke-setting');
		paint.show('fill-setting');
		paint.show('join-setting');
		paint.hide('close-path-setting');
	}else if(element.id == "draw-ellipse"){
		paint.show('line-thickness-setting');
		paint.show('stroke-setting');
		paint.show('fill-setting');
		paint.hide('join-setting');
		paint.hide('close-path-setting');
	}else{
		paint.show('close-path-setting');
		paint.show('line-thickness-setting');
		paint.show('stroke-setting');
		paint.show('fill-setting');
		paint.show('join-setting');
	}
}

//Helper function
function setStrokeState(element, paint){
	if(paint.getStroke() == null){
		console.log("Setting stroke swatch to disabled");
		element.style.backgroundImage = 'images/icon_disabled.jpg';
		element.style.width = "40px";
		element.style.height = "40px";
	}
}

function ol(){
	// NOTE: An even better approach than the one below would be to actually
	// attach the instance of Paint to a div, complete with a UI and the canvas.
	// The div itself could be a complete package, allowing us to easily
	// have many different Paint instances on a single page.

	var canvas=document.getElementById("theCanvas");
	paint=new Paint(canvas);
	paint.strategy=new SquiggleStrategy(paint); // for now, the default strategy

	// FIX THIS so that the strategy comes from the UI
	canvas.addEventListener("mousedown", function(event){ paint.strategy.mousedown(paint.mapToCanvas(event)); }, false);
	canvas.addEventListener("mouseup",   function(event){ paint.strategy.mouseup(paint.mapToCanvas(event)); }, false);
	canvas.addEventListener("mousemove", function(event){ paint.strategy.mousemove(paint.mapToCanvas(event)); }, false);
	canvas.addEventListener("mouseout",  function(event){ paint.strategy.mouseout(paint.mapToCanvas(event)); }, false);

	//listen for window resize
	window.onresize=function(){paint.draw();}

	//Start navigation toggle listener
	navToggler(document.getElementsByClassName('tool'));

	//hide some default settings since brush is the default strategy
	paint.hide('fill-setting'); 
	paint.hide('close-path-setting');
	paint.hide('join-setting');

	//listen for set no fill
	document.getElementById('set-no-fill').addEventListener('click', function(event){
		var element = document.getElementById('small-fill-picker');
		element.style.backgroundImage = "url('images/icon_disabled.jpg')";
		element.style.width = "40px";
		element.style.height = "40px";
	});

	//listen for set no stroke
	document.getElementById('set-no-stroke').addEventListener('click', function(event){
		var element = document.getElementById('small-stroke-picker');
		console.log("Setting fill swatch to disabled: " + element);
		element.style.backgroundImage = "url('images/icon_disabled.jpg')";
		element.style.width = "40px";
		element.style.height = "40px";
	});


	// QUESTION: Why does the following NOT work
	//canvas.addEventListener("mouseout",  paint.strategy.mouseout,  false);
	//if you move the mouse outside the boundaries the even does not fire on the canvas, it fires on the 
	//html document; to fix it you map the points onto the canvas using the canvas starting x,y points as offsets 
	//and pass the modified event to the canvas
}
