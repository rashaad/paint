<?php
	header('Content-Type: application/json');
	try {
  		require('constants.inc'); // defines $connection_string
  		// open connection to MongoDB server
  		$conn = new Mongo($connection_string);
		
  		// access database
  		$db = $conn->$my_default_db;
		
  		// access collection
  		$collection = $db->$my_collection;
		
    		// retrieve existing document 
  		$criteria = array();
  		$fields = array( '_id' => true,);
  		$cursor = $collection->find($criteria, $fields);
		$all_worlds=array();
		foreach($cursor as $doc){
			$all_worlds[]=$doc['_id'];
		}
		print json_encode($all_worlds);
  		
  		// disconnect from server
  		$conn->close();
	} catch (MongoConnectionException $e) {
  		die('Error connecting to MongoDB server');
	} catch (MongoException $e) {
  		die('Error: ' . $e->getMessage());
	}
?>
