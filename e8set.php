<?php
	header('Content-Type: application/json');
	$update=json_decode($_REQUEST['update']);

	try {
  		require('constants.inc'); // defines $connection_string
  		// open connection to MongoDB server
  		$conn = new Mongo($connection_string);
		
  		// access database
  		$db = $conn->$my_default_db;
		
  		// access collection
  		$collection = $db->$my_collection;
		
    	// retrieve existing document 
  		$criteria = array( '_id' => $update->world,);
  		$doc = $collection->findOne($criteria);
		$doc['_id']=$update->world;
		foreach($update as $key=>$value){
			if(array_key_exists($key,$doc['data'])){
				$value->sequenceNumber=$doc['data'][$key]['sequenceNumber']+1;
  				$doc['data'][$key] = $value;
			} else {
				$value->sequenceNumber=1;
  				$doc['data'][$key] = $value;
			}
		}
  		$collection->save($doc);
  		
  		// disconnect from server
  		$conn->close();
	} catch (MongoConnectionException $e) {
  		die('Error connecting to MongoDB server');
	} catch (MongoException $e) {
  		die('Error: ' . $e->getMessage());
	}

	print json_encode($world);
?>
