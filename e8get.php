<?php
	header('Content-Type: application/json');
	try {
  		require('constants.inc'); // defines $connection_string
  		// open connection to MongoDB server
  		$conn = new Mongo($connection_string);
		
  		// access database
  		$db = $conn->$my_default_db;
		
  		// access collection
  		$collection = $db->$my_collection;
		
    		// retrieve existing document 
  		$criteria = array( '_id' => $_REQUEST['worldID'],);
  		$doc = $collection->findOne($criteria);
		print json_encode($doc['data']);
  		
  		// disconnect from server
  		$conn->close();
	} catch (MongoConnectionException $e) {
  		die('Error connecting to MongoDB server');
	} catch (MongoException $e) {
  		die('Error: ' . $e->getMessage());
	}
?>
