var intervalGetWorld;
var currentWorldID="testing";

function Paint(){
	this.stage=new Kinetic.Stage({
        container: 'canvas',
        width: $(window).width(),
        height: $(window).height()
    });

	this.canvas=new Kinetic.Layer(); // The canvas this instance of Paint is working on
	this.context=this.canvas.getContext();

 	// Strategy design pattern. 
	// strategy is used to map some canvas events to operations on a command being constructed
	// change strategies to construct a SquiggleCommand, a RectangleCommand, a CircleCommand, ...
	this.strategy=null;


	//Settings variables
	this.fillColor = "rgba(0,0,0,1)"; //fill color
	this.strokeColor = "rgba(0,0,0,1)"; //stroke color
	this.lineJoinType = 'miter'; //type of join for lines
	this.lineThicknessEnabled = true;
	this.lineThickness = 1; //thickness of stroke
	this.closePath = true;

	//create the color picker
	//this.picker = new ColorPicker('color-picker', 'color-preview', 'color-picked', 'images/color_picker.png', 256,256);

}

Paint.prototype.setStrategy=function(strategy){
	this.strategy = strategy;
}

Paint.prototype.add=function(shape){
	console.log(shape);
	this.canvas.add(shape);
	this.draw();
}

Paint.prototype.draw=function(){
	this.stage.removeChildren();
	this.stage.add(this.canvas)
}

Paint.prototype.undo=function(){
	this.canvas.children.pop();
	this.draw();
}

Paint.prototype.clear=function(){
	this.canvas.removeChildren();
	this.draw();
}

function RectangleStrategy(paint){
	this.paint = paint; // the instance of paint this strategy applies to
	this.startPoint=null;
	this.endPoint=null;
	this.width=null;
	this.height=null;
}

RectangleStrategy.prototype.draw=function(){
	this.width=this.endPoint.x - this.startPoint.x;
	this.height=this.endPoint.y - this.startPoint.y;
	var rect = new Kinetic.Rect({
		x: this.startPoint.x,
		y: this.startPoint.y,
		width: this.width,
		height: this.height,
		fill: this.paint.fillColor,
		stroke: this.paint.strokeColor,
		strokeWidth: this.paint.lineThickness,
		draggable: true
	});
	this.paint.add(rect);	
}

RectangleStrategy.prototype.touchstart=function(){

	// do this only if user didn't click on existing shape
	shapeSelected=this.paint.stage.getIntersections(this.paint.stage.getTouchPosition()).length>0;
	if(!shapeSelected){		
		this.startPoint=this.paint.stage.getTouchPosition();
		this.endPoint=this.startPoint;
		this.draw();
	}
}

RectangleStrategy.prototype.touchmove=function(){
	if (this.startPoint!=null){
		this.paint.undo(); //whatever you drew previously, remove it until user stops moving mouse
		this.endPoint=this.paint.stage.getTouchPosition();
		this.draw();			
	}
}

RectangleStrategy.prototype.touchend=function(){
	if (this.startPoint!=null){
		this.paint.undo(); //remove previous drawing from mousemove
		this.endPoint=this.paint.stage.getTouchPosition();
		this.draw(); //final rectangle
		this.startPoint=null;
	}
	
}

RectangleStrategy.prototype.mousedown=function(){

	// do this only if user didn't click on existing shape
	shapeSelected=this.paint.stage.getIntersections(this.paint.stage.getMousePosition()).length>0;
	if(!shapeSelected){		
		
		this.startPoint=this.paint.stage.getMousePosition();
		this.endPoint=this.startPoint;
		this.draw();

	}
}

RectangleStrategy.prototype.mousemove=function(){

	if (this.startPoint!=null){
		this.paint.undo(); //whatever you drew previously, remove it until user stops moving mouse
		this.endPoint=this.paint.stage.getMousePosition();
		this.draw();			
	}
}

RectangleStrategy.prototype.mouseup=function(){
	if (this.startPoint!=null){
		this.paint.undo(); //remove previous drawing from mousemove
		this.endPoint=this.paint.stage.getMousePosition();
		this.draw(); //final rectangle
		this.startPoint=null;
	}
	
}

RectangleStrategy.prototype.mouseout=function(){

}

function ol(){

	paint = new Paint();

	//load shapes onto paint stage from db
	// set interval for loading shapes from db
	//intervalGetWorld = window.setInterval(getWorld, 1000);

    canvas=paint.stage.content;
    canvas.addEventListener("mousedown", function(event){ paint.strategy.mousedown(); }, false);
	canvas.addEventListener("mouseup",   function(event){ paint.strategy.mouseup(); }, false);
	canvas.addEventListener("mousemove", function(event){ paint.strategy.mousemove(); }, false);
	canvas.addEventListener("mouseout",  function(event){ paint.strategy.mouseout(); }, false);
	canvas.addEventListener("touchstart", function(event){ paint.strategy.touchstart(); }, false);
	canvas.addEventListener("touchmove",   function(event){ paint.strategy.touchmove(); }, false);
	canvas.addEventListener("touchend", function(event){ paint.strategy.touchend(); }, false);

	paint.setStrategy(new RectangleStrategy(paint)); 

}